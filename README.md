# MAPAS CONCEPTUALES

## MAPA 1: Programación Declarativa. Orientaciones y pautas para el estudio (1998, 1999, 2001)

```plantuml
@startmindmap
caption Mapa Conceptual 1
<style>
mindmapDiagram {
    node {
        LineColor black
    }
    rootNode {
        BackgroundColor orange
        FontColor black
        LineColor black
    }
    boxless {
    FontColor black
  }
    :depth(2) {
      BackGroundColor lightgreen
      FontColor black
    }
    :depth(3) {
      BackGroundColor #FF4444
      FontColor black
    }
    :depth(4) {
      BackGroundColor #FF4444
      FontColor black
    }
    :depth(5) {
      BackGroundColor #44FFE6
      FontColor black
    }
    :depth(6) {
      BackGroundColor #BD76FC
      FontColor black
    }
    :depth(8) {
      BackGroundColor #AFAFAF
      FontColor black
    }
    arrow {
    LineColor black
    LineThickness 1.5
}
}
</style>
*:Programacion
declarativa;
**_ ¿Que es?
*** Es un estilo de programación
****_ Surge 
*****:como reacción a algunos 
problemas que lleva consigo 
la programación imperativa(clásica);
****_ Consiste
*****:En que las tareas rutinarias de 
programación se dejan al compilador;
*****:en abandonar las secuencias de 
órdenes que gestionan la memoria del ordenador;
******_ Para esto
*******:Toman una perspectiva de darle 
explicaciones al ordenador de como 
son los algoritmos;
**_ Su idea principal es
***:Liberarse de las
asignaciones;
***:Liberarse de tener que detallar 
específicamente cual es el control de 
la gestión de memoria en el ordenador;
****_ Y
*****:Utilizar otros recursos que permitan 
especificar los programas a un nivel más alto;
******_ Para que
*******:Para que este más cercano a 
la forma de pensar del programador;
**_ La programacion se vivide en 2 partes
*** Parte creativa
****_ Es donde
***** Se plantea la idea
***** Se plantea el algoritmo
***** Se resuelve el problema
****_ Se busca
***** Ahorrar tiempo
***** Ahorrar recursos
*** Parte burocratica
****:El compilador se encarga 
de hacer esta parte;
****_ Es donde 
*****:Se hace la gestión detallada 
de la memoria del ordenador;
******_ Para seguir
*******:Una secuencia estricta 
de órdenes;
********_ Esto se hace 
*********:Para que el ordenador ejecute 
correctamente nuestras ideas;
**_ Ventajas
*** Programas más cortos
*** Programas más fáciles de depurar
*** Se reduce la complejidad de los programas
*** Se reduce el riesgo de cometer errores
**_ Desventajas
*** En parte, difícil de comprender para personas ajenas
*** Basado en una forma de pensar no habitual en las personas
*** No puede resolver cualquier problema dado
****_ porque 
*****:Está restringida al subconjunto de 
problemas para los que el 
intérprete o compilador fue diseñado;
**_ Variantes principales
*** Programacion Funcional
****:Recurre al lenguaje que utilizan 
los matemáticos cuando describen funciones;
*****_ Uitliza
****** Reglas de reescritura
****** Simplificacion de expresiones
**** Se utiliza el razonamiento ecuazional
****_ Caracteristicas
*****:Existencia de funciones
de orden superior;
******:Se pueden aplicar programas 
a otros programas que 
modifican otros programas;
******:No hay distinción 
entre datos y programas;
***** Evaluacion perezosa
******_ Consiste 
*******:En que los cálculos no 
se realizan hasta que otro 
posterior lo necesita;
****_ Lenguajes utilizados
***** Haskell
******:Es el lenguaje estandar de 
este tipo de paradigma;
***** Lisp
******:Es un lenguaje hibrido que permite 
hacer un tipo de programación imperativa;
*** Programacion Lógica
****:Recurre a la lógica de 
predicados de primer orden;
*****_ Se utilizan
****** Axiomas
****** Reglas de inferencia
*****:El compilador es 
un motor de inferencia;
******_ Que
*******:A partir de enunciados razona 
y da respuesta a nuestras preguntas;
****_ Caracteristicas
*****:Tenemos relaciones entre 
objetos que se están definiendo;
***** Permite ser más declarativos
*****:Se emplea habitualmente en 
la inteligencia artificial;
****_ Lenguaje utilizado
***** PROLOG
******_ ¿Que es?
*******:Es un lenguaje de programación 
lógico e interpretado;
********_ Usado habitualmente en 
********* El campo de la Inteligencia artificial
center footer Torres Sanchez Usiel Alberto
@endmindmap
```

## MAPA 2: Lenguaje de Programación Funcional (2015)
```plantuml
@startmindmap
caption Mapa Conceptual 2
<style>
mindmapDiagram {
    node {

        BackgroundColor #FFC353
        FontColor black
        LineColor black
    }
    rootNode {
        BackgroundColor #C2FF78
        FontColor black
        LineColor black
    }
    boxless {
    FontColor black
    }
    arrow {
    LineColor black
  }
}
</style>
* Lenguaje de Programación Funcional
** ¿Que es un programa?
*** Es una coleccion de datos
*** Una serie de instrucciones
****_ Que
*****: Operan sobre 
dichos datos;
*****: Estas instrucciones se
ejecutan en un orden;
*****_ que se modifica segun
******: El valor de 
las variables;
** Paradigmas de programacion
***_ Es
**** Un modelo de computacion
*****_ En el que
******:Existen diferentes
 lenguajes;
*******_ Que 
********: Dotan 
de semántica;
*********_ A 
********** Los programas
** Modelo de Von Neumann
***: Inicialmente todos los lenguajes
estaban basados en este modelo;
*** En honor a John von Neumann
****_ Propuso que
*****: Los programas 
debían almacenarse
 en la misma maquina;
******_ Esto debia hacerse
*******: Antes de 
ser ejecutados;
*****: La ejecución 
consistiría en 
una serie de 
instrucciones;
******_ Que se 
*******: Irían ejecutando 
secuencialmente;
********_ Las secuencias se modifican segun
********* El estado del computo
********** Es una zona de memoria
***********_ A la que
************: Se accede mediante 
una serie de variables;
*************: Almacena el valor de todas 
las variables que se 
definen en el programa;
**************:Estas variables se pueden 
modificar mediante 
una instrucción de asignación;
**: Programacion orientada
a objetos (poo);
***_ ¿Que es?
****: Son pequeños trozos
de codigo llamados objetos
que van interactuando entre si;
*****: Esos objetos se 
componen de instrucciones;
******_ Que se
******* Ejecutan secuencialmente
**: Paradigma de programacion
logico;
***_ Surge
**** Lambda Calculo
*****: Fue introducido por 
Alonzo Church y Stephen Kleene;
******_ En
******* 1930
*****: Es equivalente 
al modelo de Neumann;
*****_ Es
****** Un sistema formal
*******_ Diseñado para investigar
******** La definición de función
********: La noción de 
aplicación de funciones;
******** La recursion
*****_ Fue la base para
****** La lógica combinatoria
******* La desarrollo Haskell Brooks Curry
*******_ Acento las bases para
******** La programacion funcional
********* Ventajas
********** Es muy eficaz
***********_ Sus programas son 
************ Rapidos
************ Cortos
********** Utiliza recursion
**********: Si se usan los
mismos parametros de entrada;
***********: Va a devolver 
el mismo valor;
*********_ Se caracteriza por
********** Nos vamos a encontrar con funciones 
**********: Los programas solo van a depender 
de los parámetros de entrada;
**********: Las funciones pueden ser 
parámetros de otras funciones;
********* Evaluacion de Funciones
********** Evaluacion estricta
***********_ Es
************ Facil de implementar
***********: Primero evalua 
lo mas interno
del codigo;
************_ Y despues
************* Evalua lo mas externo
********** Evaluacion no estricta
***********_ Es
************ Dificil de implementar
***********: Primero evalua 
lo mas externo
del codigo;
************_ Y despues
************* Evalua lo mas interno
********** Evaluacion perezosa
***********_ Consiste 
************:En que los cálculos no 
se realizan;
*************_ hasta que 
**************:otro posterior 
lo necesita;
***************_ A esto se le\n conoce como
**************** Paso por necesidad
***********_ Utiliza
************ Memorizacion
*************_ Consiste
**************: En almacenar el valor
de una expresion;
***************_ Que
**************** Ya fue calculada
** Conceptos importantes
*** Variables
****_ Es
***** Un valor que puede cambiar
****_ Son
*****: Los parametros
 de entrada;
*** Constantes
****_ Es
***** Un valor fijo
****: Equipara funciones
 sin parametro;
*** Currificacion
****_ ¿Que es?
***** Es una función
******_ Que tiene
******* Varios parametros de entrada
********_ Y devuelve
********* Diferentes salidas (funciones)
****_ En honor a 
***** Haskell Brooks Curry
center footer Torres Sanchez Usiel Alberto
@endmindmap
```